﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace FloatMultiplication
{
    class Program
    {
        public static long addition(long binary1, long binary2)
        {
            int i = 0, remainder = 0;
            int[] sum = new int[20];
            long binarySum = 0;

            while (binary1 != 0 || binary2 != 0)
            {
                sum[i++] = (int)(binary1 % 10 + binary2 % 10 + remainder) % 2;
                remainder = (int)(binary1 % 10 + binary2 % 10 + remainder) / 2;
                binary1 = binary1 / 10;
                binary2 = binary2 / 10;
            }

            if (remainder != 0)
                sum[i++] = remainder;
            i--;


            while (i >= 0)
                binarySum = binarySum * 10 + sum[i--];

            return binarySum;
        }

        public static long convertFunction(int int1, float fraction)
        {
            int i;
            int Base = 1;
            int remainder,digit;
            long binary = 0;
            while (int1 > 0)
            {
                remainder = int1 % 2;
                binary = binary + remainder * Base;
                Base = Base * 10;
                int1 = int1 / 2;
            
            for (i = 1; i <= 3; i++)
            {

                fraction = fraction * 2;
                digit = (int)fraction;
                fraction = fraction - digit;
                binary = binary * 10 + digit;
            }
            return binary;
        }

        static void Main(string[] args)
        {
            float a = float.Parse(Console.ReadLine());
            float b = float.Parse(Console.ReadLine());
            int a1 = Convert.ToInt32(a);
            int b1 = Convert.ToInt32(b);
            float fractional1 = a - a1;
            float fractional2 = b - b1;
            long binary1 = convertFunction(a1, fractional1);
            long binary2 = convertFunction(b1, fractional2);
            //Console.WriteLine(binary1);
            //Console.WriteLine(binary2);
            int digit, factor = 1, num;
            long product = 0;

            while (binary2 != 0)
            {
                digit = (int)binary2%10;

                if (digit == 1)
                {
                    binary1 = binary1 * factor;
                    product = addition(binary1, product);
                }
                else
                    binary1 = binary1 * factor;

                binary2 = binary2 / 10;
                factor = 10;
            }

            float sum1 = 0, sum2 = 0, k;
            for (k = 6; k >= 1; k--)
            {
                num = (int)product % 10;
                sum1 = sum1 + (num / ((int)Math.Pow(2, k)));
                product = product / 10;
            }
            while (product != 0)
            {
                num = (int)product % 10;
                sum2 = sum2 + (num * ((int)Math.Pow(2, k)));
                k++;
                product = product / 10;
            }
            
            Console.WriteLine(sum1 + sum2);
            
            Console.ReadKey();
            

        }
    }
}
