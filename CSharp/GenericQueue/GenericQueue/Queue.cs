﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericQueue
{
    class GQueue<T>
    {
        public int rear = -1;
        public int front = -1;
        T[] queue;
        public int size;
        public GQueue(int capacity)
        {
            size = capacity;
            queue = new T[size];

        }
        public void Enqueue(T element)
        {
            if (rear == size - 1)
            {
                throw new Exception();
            }
            else
            {
                if (front == -1)
                {
                    front = 0;
                }
                rear++;
                queue[rear] = element;
            }
        }
        public void Dequeue()
        {
            if(front==-1 || front > rear)
            {
                throw new Exception();
            }
            else
            {
                front = front + 1;
            }
        }
        public void print()
        {
            if (front == -1)
            {
                Console.WriteLine("Queue is Empty");
            }
            else
            {
                for(int i = front; i <= rear; i++)
                {
                    Console.Write(queue[i] + " | ");
                }
                Console.WriteLine();
            }
        }
    }
}
