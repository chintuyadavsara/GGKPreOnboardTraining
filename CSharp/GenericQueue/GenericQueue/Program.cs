﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int size;
                Console.WriteLine("Enter Size of queue :");
                size = int.Parse(Console.ReadLine());
                GQueue<int> queue = new GQueue<int>(size);
                while (true)
                {
                    Console.WriteLine("1.Enqueue");
                    Console.WriteLine("2.Dequeue");
                    Console.WriteLine("3.Print queue");
                    Console.WriteLine("4. Exit");
                    Console.WriteLine("Eneter your Choice :");
                    int choice = int.Parse(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Enter Number to Enqueue :");
                            int temp = int.Parse(Console.ReadLine());
                            queue.Enqueue(temp);
                            break;
                        case 2:
                            queue.Dequeue();
                            
                            break;
                        case 3:
                            queue.print();
                            break;
                        case 4:
                            System.Diagnostics.Process.GetCurrentProcess().Kill();//to kill the process
                            break;
                        default:
                            Console.WriteLine("Invalid Input");
                            break;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Queue size either Exceeded");
                Console.ReadKey();
            }
            
        }
    }
}