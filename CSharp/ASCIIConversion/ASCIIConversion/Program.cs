﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASCIIConversion
{
    class Program
    {
        public static bool isPrime(int num)
        {
            if (num >= 1 && num <= 3)
            {
                return true;
            }
            if (num % 2 == 0)
            {
                return false;
            }
            for (int i = 3; i < num / 2; i = i + 2)
            {
                if (num % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        static void Main(string[] args)
        {
            String name = Console.ReadLine();
            int nameLength = name.Length;
            
            int[] FinalResult=new int[nameLength];
            int[] BeforeFinal=new int[nameLength];
            int indexF = 0;
            int indexB = 0;
            StringBuilder ConvertedString = new StringBuilder();
            for (int i=0;i<nameLength-1;i++)
            {
                int k = name[i];
                int k1 = name[i + 1];
                int average = (k + k1) / 2;
                //BeforeFinal = BeforeFinal + (average.ToString());
                BeforeFinal[indexB++] = average;
                if (!isPrime(average))
                {
                    //FinalResult = FinalResult + (average.ToString());
                    FinalResult[indexF++] = average;
                    //Console.Write(average+ " ");
                }
                else
                {
                    //FinalResult = FinalResult + ((average+1).ToString());
                    FinalResult[indexF++] = average+1;
                    //Console.Write(average + 1 + " ");
                }
            }
            for(int i = 0; i < indexB; i++)
            {
                Console.Write(BeforeFinal[i]+" ");
            }
            Console.WriteLine();
            for (int i = 0; i < indexF; i++)
            {
                Console.Write(FinalResult[i]+" ");
            }
            Console.WriteLine();
            
            for (int i = 0; i < indexF; i++)
            {
                //FinalString =;
                ConvertedString.Append((char)FinalResult[i]);
                //Console.Write((char)FinalResult[i]);
            }
            name = ConvertedString.ToString();
            Console.WriteLine(name);
            Console.ReadKey();
        }
    }
}
