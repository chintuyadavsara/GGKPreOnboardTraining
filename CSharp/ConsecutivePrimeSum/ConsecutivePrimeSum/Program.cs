﻿using System;

namespace ConsecutivePrimeSum
{
    class ConsecutivePrimeSum
    {
        public static bool isPrime(int num)
        {
            if (num>=1 && num<=3)
            {
                return true;
            }
            if (num%2==0)
            {
                return false;
            }
            for (int i=3; i<num/2; i=i+2)
            {
                if (num%i==0)
                {
                    return false;
                }
            }
            return true;
        }

        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int sum = 2;
            int count = 0;
            for(int j=3;j<n;j++)
            {
                if(isPrime(j))
                {
                    sum = sum + j;
                    if(isPrime(sum) && (sum<n))
                    {
                        count++;
                    }
                }
            }
            Console.WriteLine(count);
            Console.ReadKey();
        }
    }
}
