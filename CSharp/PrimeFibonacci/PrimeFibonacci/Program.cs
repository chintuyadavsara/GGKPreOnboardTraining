﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeFibonacci
{
    class Program
    {
        public static bool isPrime(long num)
        {
            if (num >= 1 && num <= 3)
            {
                return true;
            }
            if (num % 2 == 0)
            {
                return false;
            }
            for (int i = 3; i < num / 2; i = i + 2)
            {
                if (num % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        static void Main(string[] args)
        {
            long number = long.Parse(Console.ReadLine());
            long a = 2;
            long b = 3;
            long[] fibArray = new long[number];
            long index = 0;
            for (long i = 0; i < number; i = i + 2)
            {
                if (isPrime(a))
                {
                    fibArray[index++] = a;
                }
                if (b < number && isPrime(b))
                {
                    fibArray[index++] = b;
                }
                a = a + b;
                b = b + a;
            }
            for (long i = 0; i < index + 1; i++)
            {
                for (long j = 0; j < i; j++)
                {
                    Console.Write(fibArray[j] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}