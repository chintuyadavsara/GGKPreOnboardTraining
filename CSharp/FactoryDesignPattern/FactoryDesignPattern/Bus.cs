﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDesignPattern
{
    class Bus : IVehicle
    {
        public override void build(string model)
        {
            Console.WriteLine("My school bus was {0} model bus", model);
        }
    }
}
