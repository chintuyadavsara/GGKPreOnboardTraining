﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDesignPattern
{
    public class Truck : IVehicle
    {
        public override void build(string model)
        {
            Console.WriteLine("{0} truck is carrying sand", model);
        }
    }
}
