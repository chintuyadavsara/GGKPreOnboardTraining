﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDesignPattern
{
    class Car : IVehicle
    {
        public override void build(string model)
        {
            Console.WriteLine("I own a {0} car", model);
        }
    }
}
