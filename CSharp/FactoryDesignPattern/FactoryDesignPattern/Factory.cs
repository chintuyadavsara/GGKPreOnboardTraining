﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDesignPattern
{
    public class Factory
    {
        public IVehicle buildVehicle(string vehicleType)
        {
            if (string.Equals(vehicleType, "Car", StringComparison.OrdinalIgnoreCase))
            {
                IVehicle vehicle = new Car();
                return vehicle;
            }
            else if (string.Equals(vehicleType, "Bus", StringComparison.OrdinalIgnoreCase))
            {
                IVehicle vehicle = new Bus();
                return vehicle;
            }
            else if (string.Equals(vehicleType, "Truck", StringComparison.OrdinalIgnoreCase))
            {
                IVehicle vehicle = new Truck();
                return vehicle;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        static void Main(string[] args)
        {
            Factory factoryObj = new Factory();
            IVehicle vehicle = factoryObj.buildVehicle("Car");
            vehicle.build("Tesla");

            vehicle = factoryObj.buildVehicle("Bus");
            vehicle.build("Tata Motors");

            vehicle = factoryObj.buildVehicle("Truck");
            vehicle.build("Swaraj Mazda");
            Console.ReadKey();
        }
    }
}
