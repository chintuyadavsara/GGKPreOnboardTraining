﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericStack
{
    class GStack<T>
    {
        public int size;
        public T[] stack;
        public int top;
        public GStack(int capacity)
        {
            size = capacity;
            stack = new T[size];
            top = -1;
        }
        public void push(T element)
        {
            if (top == size - 1)
            {
                throw new StackOverflowException();
            }
            else
            {
                top = top + 1;
                stack[top] = element;
            }   
        }
        public T pop()
        {
            T RemovedElement;
            T temp = default(T);
            
            if (!(top <= 0))
            {
                RemovedElement = stack[top];
                top = top - 1;
                return RemovedElement;
            }
            else
            {
                throw new Exception();
            }
            return temp;

        }
        
        public int getSize()
        {
            return top+1;
        }
        public void print()
        {
            T[] Elements = new T[top + 1];
            Array.Copy(stack, 0, Elements, 0, top + 1);
            for(int i = 0; i < Elements.Length; i++)
            {
                Console.Write(Elements[i] +" | ");
            }
            Console.WriteLine();
        }
    }
}
