﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericStack
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int size;
                Console.WriteLine("Enter Size of Stack :");
                size = int.Parse(Console.ReadLine());
                GStack<int> stack = new GStack<int>(size);
                //remove stack size if stack to be dynamic
                while (true)
                {
                    Console.WriteLine("1.Push");
                    Console.WriteLine("2.Pop");
                    Console.WriteLine("3.Print Stack");
                    Console.WriteLine("4. Exit");
                    Console.WriteLine("Eneter your Choice :");
                    int choice = int.Parse(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Enter Number to Push :");
                            int temp = int.Parse(Console.ReadLine());
                            stack.push(temp);
                            break;
                        case 2:
                            int pop = stack.pop();
                            Console.WriteLine("poped element->"+ pop);
                            break;
                        case 3:
                            stack.print();
                            break;
                        case 4:
                            System.Diagnostics.Process.GetCurrentProcess().Kill();//to kill the process
                            break;
                        default:
                            Console.WriteLine("Invalid Input");
                            break;
                    }
                }
                
            }
            catch(StackOverflowException)
            {
                Console.WriteLine("StackOverflow");
            }
            catch (Exception)
            {
                Console.WriteLine("StackUnderflow");
            }
        }
    }
}
